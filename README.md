# Test Sber project

### Подготовка перед запуском проекта
1. Выполните команду ```docker-compose up``` в корне проекта.
2. Откройте adminer [http://localhost:8888/](http://localhost:8888/).
```
    username: postgres
    password: 123456Q
```
3. Создайте базу данных ```counter```.

### Работа с REST API
1. Вы можете открыть swagger по ссылке [http://localhost:8080/swagger-ui/index.html#](http://localhost:8080/swagger-ui/index.html#).
