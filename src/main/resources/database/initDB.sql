CREATE TABLE IF NOT EXISTS counters
(
    id         BIGSERIAL        PRIMARY KEY,
    name       varchar(255)     NOT NULL    UNIQUE,
    value      BIGINT           NOT NULL
);