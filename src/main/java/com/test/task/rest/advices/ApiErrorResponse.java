package com.test.task.rest.advices;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

/**
 * Класс предстовляет возвраащаемы объект бъект ошибки API.
 */
@Getter
@Setter
@NoArgsConstructor
class ApiErrorResponse {

    /**
     * Создает новый экземпляр ответа об ошибке API.
     * @param status - HTTP статус код, связанный с ошибкой.
     * @param message - Сообщение об ошибке.
     */
    ApiErrorResponse(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    /**
     * HTTP статус код, связанный с ошибкой.
     */
    private HttpStatus status;

    /**
     * Сообщение об ошибке.
     */
    private String message;
}