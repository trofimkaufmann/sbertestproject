package com.test.task.rest.dto.counter;

import com.test.task.domain.counter.CounterModel;
import com.test.task.rest.dto.DtoMapper;
import org.mapstruct.Mapper;

/**
 * Mapper для преобразования объекта модели счётчика в DTO объект чтения счётчика и наоборот.
 */
@Mapper(componentModel = "spring")
public interface CounterReadDtoMapper extends DtoMapper<CounterReadDto, CounterModel> {

    /**
     * Преобразует объект DTO в объект модели.
     *
     * @param dto - Объект DTO.
     * @return Объект модели.
     */
    CounterModel dtoToModel(CounterReadDto dto);

    /**
     * Преобразует объект модели в объект DTO.
     *
     * @param model - Объект модели.
     * @return Объект DTO.
     */
    @Override
    CounterReadDto modelToDto(CounterModel model);
}
