package com.test.task.rest.dto.counter;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * DTO с полями для чтения счётчика.
 */
@Getter
@Setter
@NoArgsConstructor
public class CounterReadDto {

    /**
     * Имя счётчика.
     */
    @ApiModelProperty(value = "Имя счётчика")
    private String name;

    /**
     * Значение счётчика.
     */
    @ApiModelProperty(value = "Значение счётчика")
    private Long value;
}
