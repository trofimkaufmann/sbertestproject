package com.test.task.rest.dto.counter;

import com.test.task.domain.counter.CounterModel;
import com.test.task.rest.dto.DtoMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper для преобразования объекта модели счётчика в DTO объект создания счётчика и наоборот.
 */
@Mapper(componentModel = "spring")
public interface CounterCreateDtoMapper extends DtoMapper<CounterCreateDto, CounterModel> {

    /**
     * Преобразует объект DTO в объект модели.
     *
     * @param dto - Объект DTO.
     * @return Объект модели.
     */
    @Mapping(target = "value", ignore = true)
    CounterModel dtoToModel(CounterCreateDto dto);

    /**
     * Преобразует объект модели в объект DTO.
     *
     * @param model - Объект модели.
     * @return Объект DTO.
     */
    @Override
    CounterCreateDto modelToDto(CounterModel model);
}
