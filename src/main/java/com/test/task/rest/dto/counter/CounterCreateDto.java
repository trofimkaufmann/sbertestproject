package com.test.task.rest.dto.counter;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * DTO с полями для создания счётчика.
 */
@Getter
@Setter
@NoArgsConstructor
public class CounterCreateDto {

    /**
     * Имя счётчика.
     */
    @ApiModelProperty(required = true, value = "Имя счётчика")
    @NotNull(message = "Поле имя должно быть указано")
    private String name;
}
