package com.test.task.rest.dto;

/**
 * Общий mapper для преобразования объекта DTO в объект модели и наоборот.
 */
public interface DtoMapper<DtoType, ModelType> {
    /**
     * Преобразует объект DTO в объект модели.
     *
     * @param Dto - Объект DTO.
     * @return Объект модели.
     */
    ModelType dtoToModel(DtoType Dto);

    /**
     * Преобразует объект модели в объект DTO.
     *
     * @param model - Объект модели.
     * @return Объект DTO.
     */
    DtoType modelToDto(ModelType model);
}
