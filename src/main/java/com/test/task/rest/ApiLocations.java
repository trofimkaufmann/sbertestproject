package com.test.task.rest;

/**
 * Вспомогательный класс, содержащий статические значения для путей REST API.
 */
public class ApiLocations {

    /**
     * Глобальный префикс для URL-адреса API.
     */
    public static final String V1 = "/v1";

    /**
     * Путь для Счётчик API.
     */
    public static final String COUNTER_API_PATH = "/counters";
}
