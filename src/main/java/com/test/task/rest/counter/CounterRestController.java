package com.test.task.rest.counter;

import com.test.task.domain.counter.CounterService;
import com.test.task.rest.ApiLocations;
import com.test.task.rest.dto.counter.CounterCreateDto;
import com.test.task.rest.dto.counter.CounterCreateDtoMapper;
import com.test.task.rest.dto.counter.CounterReadDto;
import com.test.task.rest.dto.counter.CounterReadDtoMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Контроллер REST API, представляющий методы дляработы со счётчиками.
 */
@RestController
@RequestMapping(
        path = "${api.prefix:}" + ApiLocations.V1 + ApiLocations.COUNTER_API_PATH,
        produces = MediaType.APPLICATION_JSON_VALUE
)
public class CounterRestController {

    private final CounterService counterService;
    private final CounterCreateDtoMapper counterCreateDtoMapper;
    private final CounterReadDtoMapper counterReadDtoMapper;

    @Autowired
    public CounterRestController(
            CounterService counterService,
            CounterCreateDtoMapper counterCreateDtoMapper,
            CounterReadDtoMapper counterReadDtoMapper
    ) {
        this.counterService = counterService;
        this.counterCreateDtoMapper = counterCreateDtoMapper;
        this.counterReadDtoMapper = counterReadDtoMapper;
    }

    /**
     * Метод REST API для создания нового счётчика в базе данных.
     *
     * @param counter - Счётчик для создания
     * @return Экземпляр созданного счётчика.
     */
    @ApiOperation(
            value = "Создать счётчик"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "Счётчик успешно создан"),
            @ApiResponse(code = 400, message = "Неверные параметры запроса"),
            @ApiResponse(code = 409, message = "Счётчик с таким именем уже существует"),
            @ApiResponse(code = 500, message = "Внутренняя ошибка сервера")
    })
    @RequestMapping(
            method = RequestMethod.POST
    )
    public ResponseEntity<CounterReadDto> createCounter(
            @ApiParam(value = "Счётчик", required = true) CounterCreateDto counter
    ) {
        final CounterReadDto createdCounter = this.counterReadDtoMapper
                .modelToDto(this.counterService.create(this.counterCreateDtoMapper.dtoToModel(counter)));

        return ResponseEntity.ok(createdCounter);
    }

    /**
     * Метод REST API для инкрементирования значения счётчика в базе данных.
     *
     * @param name - Имя счётчика, значение которого будет инкрементировано.
     */
    @ApiOperation(
            value = "Инкрементировать значение счётчика"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "Значение счётчика успешно инкрементированно"),
            @ApiResponse(code = 400, message = "Неверные параметры запроса"),
            @ApiResponse(code = 404, message = "Счётчик с таким именем не найден"),
            @ApiResponse(code = 500, message = "Внутренняя ошибка сервера")
    })
    @RequestMapping(
            value = "increment/{name}",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<CounterReadDto> incrementCounterValue(
            @ApiParam(required = true, value = "Имя счётчика")
            @RequestParam(value = "name") String name) {
        final CounterReadDto incrementedCounter = this.counterReadDtoMapper
                .modelToDto(this.counterService.increment(name));
        return ResponseEntity.ok(incrementedCounter);
    }

    /**
     * Метод REST API для получения счётчика по имени из базы данных.
     *
     * @param name - Имя загружаемого счётчика из базы данных.
     * @return Экземпляр счётчика.
     */
    @ApiOperation(
            value = "Получить значение счётчика"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "Счётчик успешно получен"),
            @ApiResponse(code = 400, message = "Неверные параметры запроса"),
            @ApiResponse(code = 404, message = "Счётчик с таким именем не найден"),
            @ApiResponse(code = 500, message = "Внутренняя ошибка сервера")
    })
    @RequestMapping(
            value = "{name}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Long> getCounterValueByName(
            @ApiParam(required = true, value = "Имя счётчика")
            @PathVariable(name = "name") String name
    ) {
        return ResponseEntity.ok(this.counterService.getValueByName(name));
    }

    /**
     * Метод REST API для удаления счётчика по имени из базы данных.
     *
     * @param name - Имя удаляемого счётчика.
     */
    @ApiOperation(
            value = "Удалить счётчик"
    )
    @ApiResponses({
            @ApiResponse(code = 204, message = "Счётчик успешно удалён"),
            @ApiResponse(code = 400, message = "Неверные параметры запроса"),
            @ApiResponse(code = 404, message = "Счётчик с таким именем не найден"),
            @ApiResponse(code = 500, message = "Внутренняя ошибка сервера")
    })
    @RequestMapping(
            value = "{name}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public ResponseEntity<?> deleteCounterValueByName(
            @ApiParam(required = true, value = "Имя счётчика")
            @PathVariable(name = "name") String name
    ) {
        this.counterService.deleteByName(name);
        return ResponseEntity.noContent().build();
    }

    /**
     * Метод REST API для получения суммарного значения всех счётчиков.
     *
     * @return Суммарное значение всех счетчиков.
     */
    @ApiOperation(
            value = "Получить суммарное значение всех счетчиков"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "Суммарное значение всех счетчиков успешно получено"),
            @ApiResponse(code = 400, message = "Неверные параметры запроса"),
            @ApiResponse(code = 500, message = "Внутренняя ошибка сервера")
    })
    @RequestMapping(
            path = "/sum",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Long> getSum() {
        return ResponseEntity.ok(this.counterService.getSum());
    }

    /**
     * Метод REST API для получения уникальных имён счетчиков в виде списка.
     *
     * @return Список уникальных имён счетчиков.
     */
    @ApiOperation(
            value = "Получить список уникальных имён счетчиков"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "Список уникальных имён счетчиков успешно получено"),
            @ApiResponse(code = 400, message = "Неверные параметры запроса"),
            @ApiResponse(code = 500, message = "Внутренняя ошибка сервера")
    })
    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<String>> getNames() {
        return ResponseEntity.ok(this.counterService.getNames());
    }
}

