package com.test.task.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;

/**
 * Конфигурация спецификации Swagger 2.0 приложения.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    /**
     * Префикс API для конечных rest точек, на основе настроек профиля
     */
    @Value("${api.prefix:}")
    private String apiPrefix;

    /**
     * Создаёт сводку содержания документации.
     *
     * @return {@link Docket} с кратким изложением документации.
     */
    @Bean
    public Docket swaggerDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .forCodeGeneration(true)
                .apiInfo(apiInfo())
                .select()
                .paths(regex(apiPrefix + "/v1/.*"))
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Сбер")
                .description("Тестовое задание")
                .version("1.0.0")
                .build();
    }
}
