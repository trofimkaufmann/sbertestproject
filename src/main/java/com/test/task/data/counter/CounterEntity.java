package com.test.task.data.counter;

import com.test.task.data.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Модель сущности Счётчик.
 */
@Entity
@Table(name = "counters")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class CounterEntity extends BaseEntity {

    /**
     * Имя Счётчика.
     */
    @Column(name = "name")
    @NotNull(message = "Имя должно быть определено")
    private String name;

    /**
     * Значение Счётчика.
     */
    @Column(name = "value")
    @NotNull(message = "Значение должно быть определено")
    private Long value;
}
