package com.test.task.data.counter;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Spring Data репозиторий для класса {@link CounterEntity}.
 */
public interface CounterRepository extends JpaRepository<CounterEntity, Long> {

    Optional<CounterEntity> findByName(String name);

    @Transactional
    @Modifying
    @Query("UPDATE CounterEntity c SET c.value = c.value + 1 WHERE c.name = :name")
    void incrementByName(@Param("name") String name);

    @Query("SELECT SUM(c.value) FROM CounterEntity c")
    Optional<Long> getSum();
}
