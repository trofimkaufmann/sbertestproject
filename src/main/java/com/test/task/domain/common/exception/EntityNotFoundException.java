package com.test.task.domain.common.exception;

/**
 * Происходит, когда запрошенный объект не может быть найден.
 */
public class EntityNotFoundException extends RuntimeException {

    /**
     * Инициализирует новый экземпляр {@link EntityNotFoundException}.
     *
     * @param message - Сообщение с описанием ошибки.
     */
    public EntityNotFoundException(String message) {
        super(message);
    }
}