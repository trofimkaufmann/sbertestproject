package com.test.task.domain.common.exception;

/**
 * Происходит, когда запрос имеет недопустимые параметры.
 */
public class BadRequestException extends RuntimeException {

    /**
     * Инициализирует новый экземпляр {@link BadRequestException}.
     *
     * @param message - Сообщение с описанием ошибки.
     */
    public BadRequestException(String message) {
        super(message);
    }
}
