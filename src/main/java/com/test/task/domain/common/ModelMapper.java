package com.test.task.domain.common;

/**
 * Общий mapper для преобразования объекта модели в объект сущности и наоборот.
 */
public interface ModelMapper<ModelType, EntityType> {

    /**
     * Преобразует объект модели в объект сущности.
     *
     * @param model - Объект модели.
     * @return Объект сущности.
     */
    EntityType modelToEntity(ModelType model);

    /**
     * Преобразует объект сущности в объект модели.
     *
     * @param entity - Объект сущности.
     * @return Объект модели.
     */
    ModelType entityToModel(EntityType entity);
}
