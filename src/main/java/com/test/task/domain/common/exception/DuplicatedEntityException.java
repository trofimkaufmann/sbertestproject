package com.test.task.domain.common.exception;

/**
 * Происходит, когда объект с заданными данными уже существует в базе данных.
 */
public class DuplicatedEntityException extends RuntimeException {

    /**
     * Инициализирует новый экземпляр {@link DuplicatedEntityException}.
     *
     * @param message - Сообщение с описанием ошибки.
     */
    public DuplicatedEntityException(String message) {
        super(message);
    }
}
