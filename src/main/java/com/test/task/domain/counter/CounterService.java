package com.test.task.domain.counter;

import java.util.List;

/**
 * Предоставляет набор методов для работы со счётчиком.
 */
public interface CounterService {

    /**
     * Создаёт новый счётчик.
     *
     * @param counter - Счётчик, который будет создан в базе данных.
     * @return Счётчик, созданный в базе данных.
     */
    CounterModel create(CounterModel counter);

    /**
     * Инкрементирует значение счётчика по имени.
     *
     * @param name - Имя счётчика для инкрементации.
     * @return Счётчик с новым значением.
     */
    CounterModel increment(String name);

    /**
     * Возвращает значение счётчика по имени.
     *
     * @param name - Имя счётчика для получения значения.
     * @return Значение по указанному имени счётчика.
     */
    Long getValueByName(String name);

    /**
     * Удаляет счётчик с указанным именем.
     *
     * @param name - Имя счётчика для удаления.
     */
    void deleteByName(String name);

    /**
     * Возвращает сумму всех счётчиков.
     *
     * @return Сумму всех счётчиков.
     */
    Long getSum();

    /**
     * Возвращает список уникальных имён счётчиков.
     *
     * @return Список уникальных имён счётчико.
     */
    List<String> getNames();
}
