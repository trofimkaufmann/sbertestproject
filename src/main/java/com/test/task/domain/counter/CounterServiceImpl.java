package com.test.task.domain.counter;

import com.test.task.data.counter.CounterEntity;
import com.test.task.data.counter.CounterRepository;
import com.test.task.domain.common.exception.BadRequestException;
import com.test.task.domain.common.exception.DuplicatedEntityException;
import com.test.task.domain.common.exception.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CounterServiceImpl implements CounterService {

    private final CounterRepository repository;

    private final CounterModelMapper modelMapper;

    private static final String DUPLICATED_COUNTER_NAME_CONSTRAINT = "counters_name_key";

    @Autowired
    public CounterServiceImpl(CounterRepository repository, CounterModelMapper modelMapper) {
        this.repository = repository;
        this.modelMapper = modelMapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CounterModel create(CounterModel counterModel) {
        log.info("IN CounterServiceImpl create {}", counterModel);

        if (!StringUtils.hasText(counterModel.getName())) {
            throw new BadRequestException("Имя счётчика не указано");
        }

        try {
            return this.modelMapper.entityToModel(
                    this.repository.save(this.modelMapper.modelToEntity(counterModel)));
        } catch (DataIntegrityViolationException e) {
            throw new DuplicatedEntityException(buildErrorMessage(e, counterModel));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CounterModel increment(String name) {
        log.info("IN CounterServiceImpl increment {}", name);

        if (!StringUtils.hasText(name)) {
            throw new BadRequestException("Имя счётчика не указано");
        }

        Optional<CounterEntity> counterEntity = this.repository.findByName(name);
        if (!counterEntity.isPresent()) {
            throw new EntityNotFoundException(String.format("Счётчик с именем %s не существуе", name));
        }
        this.repository.incrementByName(name);
        counterEntity.get().setValue(counterEntity.get().getValue() + 1);
        return this.modelMapper.entityToModel(counterEntity.get());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long getValueByName(String name) {
        log.info("IN CounterServiceImpl getValueByName {}", name);

        if (!StringUtils.hasText(name)) {
            throw new BadRequestException("Имя счётчика не указано");
        }

        Optional<CounterEntity> counterEntity = this.repository.findByName(name);
        if (!counterEntity.isPresent()) {
            throw new EntityNotFoundException(String.format("Счётчик с именем %s не существуе", name));
        }
        return counterEntity.get().getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteByName(String name) {
        log.info("IN CounterServiceImpl deleteByName {}", name);

        if (!StringUtils.hasText(name)) {
            throw new BadRequestException("Имя счётчика не указано");
        }

        Optional<CounterEntity> counterEntity = this.repository.findByName(name);
        if (!counterEntity.isPresent()) {
            throw new EntityNotFoundException(String.format("Счётчик с именем %s не существуе", name));
        }
        this.repository.deleteById(counterEntity.get().getId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long getSum() {
        log.info("IN CounterServiceImpl getSum");
        Optional<Long> sum = this.repository.getSum();
        return sum.isPresent() ? sum.get() : 0L;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getNames() {
        log.info("IN CounterServiceImpl getNames");
        return this.repository.findAll().stream().map(CounterEntity::getName).collect(Collectors.toList());
    }

    private String buildErrorMessage(DataIntegrityViolationException e, CounterModel counterModel) {
        String errorMessage = e.getMessage();
        if (errorMessage.contains(DUPLICATED_COUNTER_NAME_CONSTRAINT)) {
            return "Счётчик с именем '" + counterModel.getName() + "' уже существует.";
        } else {
            return errorMessage;
        }
    }
}
