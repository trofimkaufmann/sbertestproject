package com.test.task.domain.counter;

import com.test.task.data.BaseEntity;
import com.test.task.data.counter.CounterEntity;
import com.test.task.domain.common.ModelMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper для преобразования объекта {@link CounterEntity} в объект {@link CounterModel} и наоборот.
 */
@Mapper(componentModel = "spring", uses = BaseEntity.class)
public interface CounterModelMapper extends ModelMapper<CounterModel, CounterEntity> {

    /**
     * Converts model object to entity object.
     *
     * @param model - The model object.
     * @return The entity object.
     */
    @Override
    @Mapping(target = "id", ignore = true)
    CounterEntity modelToEntity(CounterModel model);

    /**
     * Converts entity object to model object.
     *
     * @param entity - The entity object.
     * @return The model object.
     */
    @Override
    CounterModel entityToModel(CounterEntity entity);
}
