package com.test.task.domain.counter;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Модель Счётчика.
 */
@Getter
@Setter
@NoArgsConstructor
public class CounterModel {

    /**
     * Имя Счётчика.
     */
    private String name;

    /**
     * Значение Счётчика.
     */
    private Long value = 0L;
}
