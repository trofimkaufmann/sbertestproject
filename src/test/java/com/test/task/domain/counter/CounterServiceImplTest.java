package com.test.task.domain.counter;

import com.test.task.data.counter.CounterEntity;
import com.test.task.data.counter.CounterRepository;
import com.test.task.domain.common.exception.BadRequestException;
import com.test.task.domain.common.exception.DuplicatedEntityException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CounterServiceImplTest {

    @Mock
    private CounterRepository repository;

    @Mock
    private CounterModelMapper modelMapper;

    private CounterService counterService;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
        this.counterService = new CounterServiceImpl(repository, modelMapper);
    }

    @Test
    public void create_counterNotExist_createNew() {
        CounterModel counterModel = this.getTestCounterModel();
        CounterEntity counterEntity = this.getTestCounterEntity();

        doReturn(counterEntity).when(this.modelMapper).modelToEntity(counterModel);
        doReturn(counterModel).when(this.modelMapper).entityToModel(counterEntity);
        doReturn(counterEntity).when(this.repository).save(counterEntity);

        CounterModel saveCounterModel = this.counterService.create(counterModel);

        assertThat(saveCounterModel).isNotNull();

        verify(this.repository, times(1)).save(counterEntity);
    }

    @Test
    public void create_counterExists_createNew() {
        CounterModel counterModel = this.getTestCounterModel();
        CounterEntity counterEntity = this.getTestCounterEntity();

        doReturn(counterEntity).when(this.modelMapper).modelToEntity(counterModel);
        doReturn(counterModel).when(this.modelMapper).entityToModel(counterEntity);
        when(this.repository.save(counterEntity))
                .thenThrow(new DuplicatedEntityException("Test exception "));

        assertThrows(DuplicatedEntityException.class, () -> this.counterService.create(counterModel));
    }

    @Test
    public void create_nameOfCounterIsNull_createNew() {
        CounterModel counterModel = new CounterModel();
        CounterEntity counterEntity = this.getTestCounterEntity();

        doReturn(counterEntity).when(this.modelMapper).modelToEntity(counterModel);
        doReturn(counterModel).when(this.modelMapper).entityToModel(counterEntity);

        BadRequestException thrown = assertThrows(BadRequestException.class,
                () -> this.counterService.create(counterModel),
                "Ожидалось, что create() выбросит исключение - BadRequestException");

        assertTrue(thrown.getMessage().contains("Имя счётчика не указано"));
    }

    private CounterModel getTestCounterModel() {
        CounterModel counterModel = new CounterModel();
        counterModel.setName("Test");
        return counterModel;
    }

    private CounterEntity getTestCounterEntity() {
        CounterEntity counterEntity = new CounterEntity();
        counterEntity.setName("Test");
        counterEntity.setValue(0L);
        return counterEntity;
    }
}